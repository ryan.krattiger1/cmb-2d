#ifndef smtk_simulation_cmb2d_qt_qtInstanced_h
#define smtk_simulation_cmb2d_qt_qtInstanced_h

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/** CRTP Interface for creating Qt singletons
 */
template<class T>
class qtInstanced : public QObject
{
  static T* g_instance;

public:
  static T* instance(QObject* parent = nullptr)
  {
    if (!g_instance)
    {
      g_instance = new T(parent);
    }

    if (g_instance->parent() == nullptr && parent)
    {
      g_instance->setParent(parent);
    }

    return g_instance;
  }

  ~qtInstanced() override
  {
    if (g_instance == this)
    {
      g_instance = nullptr;
    }

    QObject::disconnect(this);
  }

protected:
  qtInstanced(QObject* parent = nullptr) :
    QObject(parent) {}

private:
  Q_DISABLE_COPY(qtInstanced);
};

template<class T>
T* qtInstanced<T>::g_instance = nullptr;

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
