//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

#include "smtk/io/Logger.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

// Static var to store the singleton instance
static qtProjectRuntime* g_instance = nullptr;

qtProjectRuntime* qtProjectRuntime::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new qtProjectRuntime(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

qtProjectRuntime::~qtProjectRuntime()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

std::shared_ptr<smtk::project::Project> qtProjectRuntime::project() const
{
  return m_project;
}

void qtProjectRuntime::setProject(std::shared_ptr<smtk::project::Project> p)
{
  if (m_project)
  {
    smtkErrorMacro(
      smtk::io::Logger::instance(), "Internal Error: CMB-2D cannot manage multiple projects");
    return;
  }
  m_project = p;
}

void qtProjectRuntime::unsetProject(std::shared_ptr<smtk::project::Project>)
{
  m_project.reset();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
