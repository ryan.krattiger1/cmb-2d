//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/cmb2d/qt/qtPluginLocation.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>

qtCMB2DPluginLocation::qtCMB2DPluginLocation(QObject* parent)
  : Superclass(parent)
{
}

qtCMB2DPluginLocation::~qtCMB2DPluginLocation() {}

void qtCMB2DPluginLocation::StoreLocation(const char* fileLocation)
{
  if (fileLocation == nullptr)
  {
    qDebug() << "Internal Error: pqCMB2DPluginLocation::StoreLocation() called with null pointer";
    return;
  }

  // Check whether or not this is an installed package
  QFileInfo fileInfo(fileLocation);
  QDir dirLocation(fileInfo.absoluteDir());

#ifndef NDEBUG
  qDebug() << "Plugin location: " << fileLocation;
  qDebug() << "Plugin directory: " << dirLocation.path();
#endif

  // Save starting directory for diagnostics
  QDir startingDir(dirLocation);

  // Look for path to simulation workflows
  // * Depends on superbuild-packaging organization
  // * Which is platform-dependent
  QString relativePath;
#if defined(_WIN32)
  relativePath = "../../../share/cmb/workflows/CMB2D";
#elif defined(__APPLE__)
  relativePath = "../Resources/workflows/CMB2D";
#elif defined(__linux__)
#ifdef NDEBUG
  relativePath = "../../../share/cmb/workflows/CMB2D";
#else
  // Alternate path for development
  relativePath = "../../../install/share/workflows/CMB2D";
#endif
#endif

  if (relativePath.isEmpty())
  {
#ifdef NDEBUG
    qCritical() << "Missing installed path to workflow files";
#endif
    return;
  }

  // Check if directory exists
  bool exists = dirLocation.cd(relativePath);
  if (exists)
  {
#ifndef NDEBUG
    qDebug() << "Setting workflows directory to" << dirLocation.path();
#endif
    smtk::simulation::cmb2d::Metadata::WORKFLOW_DIRECTORY = dirLocation.path().toStdString();
  }
#ifdef NDEBUG
  else
  {
    qCritical() << "No workflow directory found for starting directory" << startingDir.path()
                << "and relative path" << relativePath;
  }
#endif
}
