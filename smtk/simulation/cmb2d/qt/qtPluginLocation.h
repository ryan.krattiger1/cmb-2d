//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_qt_qtCMB2DPluginLocation_h
#define smtk_simulation_cmb2d_qt_qtCMB2DPluginLocation_h

#include <QObject>

class qtCMB2DPluginLocation : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  qtCMB2DPluginLocation(QObject* parent = nullptr);
  ~qtCMB2DPluginLocation() override;

  void StoreLocation(const char* location);

private:
  Q_DISABLE_COPY(qtCMB2DPluginLocation);
};

#endif
