set(ui_files)
set(view_sources)
set(qt_sources
  qtPluginLocation.cxx
  qtProjectRuntime.cxx)
set(moc_headers)
set(qt_headers
  qtInstanced.h
  qtPluginLocation.h
  qtProjectRuntime.h
  ${moc_headers})

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

add_library(smtkCMB2DQtExt
  ${qt_sources}
  ${view_sources}
  ${MOC_BUILT_SOURCES})

target_include_directories(smtkCMB2DQtExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:include>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${Boost_INCLUDE_DIRS})

target_link_libraries(smtkCMB2DQtExt
  PUBLIC
    smtkCMB2D
    smtkCore
    smtkPQComponentsExt
    smtkQtExt
    Qt5::Core
    Qt5::Widgets
    )

smtk_export_header(smtkCMB2DQtExt Exports.h)
smtk_install_library(smtkCMB2DQtExt)
