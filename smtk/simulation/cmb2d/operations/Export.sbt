<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="cmb2d-export" Label="CMB 2D Project - Export" BaseType="operation">
      <BriefDescription>
        Export a CMB Project. Dispatches to the simulation specific exporter to generate appropriate inputs.
      </BriefDescription>
      <AssociationsDef Name="project" NumberOfRequiredValues="1" Extensible="false" OnlyResources="true">
        <Accepts><Resource Name="smtk::project::Project"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Directory Name="export-directory" Label="Run Directory">
          <BriefDescription>
            Directory to export input files to
          </BriefDescription>
        </Directory>
        <String Name="filename" Label="Filename" Optional="true" AdvanceLevel="1">
          <BriefDescription>
            Name of the input file
          </BriefDescription>
        </String>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(cmb2d-export)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
