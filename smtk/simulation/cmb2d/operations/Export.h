#ifndef smtk_simulation_cmb2d_operations_Export_h
#define smtk_simulation_cmb2d_operations_Export_h

#include "smtk/simulation/cmb2d/Exports.h"

#include "smtk/project/Operation.h"

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class SMTKCMB2D_EXPORT Export : public smtk::project::Operation
{
public:
  smtkTypeMacro(smtk::simulation::cmb2d::Export);
  smtkCreateMacro(Export);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::project::Operation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
