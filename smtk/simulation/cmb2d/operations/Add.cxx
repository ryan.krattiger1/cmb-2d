#include "smtk/simulation/cmb2d/operations/Add.h"

#include "smtk/simulation/cmb2d/Metadata.h"

#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/mesh/core/Resource.h"
#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"

#include <algorithm>
#include <cctype>

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Error: " << message);                                             \
    return this->createResult(Add::Outcome::FAILED);                                               \
  } while (0)

#define LogWarning(message)                                                                        \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(this->log(), "Warning: " << message);                                           \
  } while (0)

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

Add::Result Add::operateInternal()
{
  auto roleStrItem = this->parameters()->findString("role");

  std::string roleStr = roleStrItem->value();
  // Filter the string if it is provided
  if (!roleStr.empty())
  {
    Metadata::Role role = Metadata::strToRole(roleStr);
    if (role == Metadata::Role::NROLE)
    {
      LogWarning("Unrecognized role: " << roleStrItem->value());
    }
    else
    {
      roleStr = Metadata::roleToStr(role);
    }
  }
  // Try to deduce the role from the input resource
  else
  {
    auto resource = this->parameters()->findResource("resource")->value();
    if (std::dynamic_pointer_cast<smtk::attribute::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::ANALYSIS);
    }
    else if (std::dynamic_pointer_cast<smtk::model::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::MODEL);
    }
    else if (std::dynamic_pointer_cast<smtk::mesh::Resource>(resource))
    {
      roleStr = Metadata::roleToStr(Metadata::MESH);
    }
    else
    {
      LogError("Role could not be deduced");
    }
  }

  auto project = this->parameters()->associations()->valueAs<smtk::project::Project>();

  // Set the role with the case fixed
  roleStrItem->setValue(roleStr);

  return smtk::project::Add::operateInternal();
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
