#include "smtk/simulation/cmb2d/operations/ImportModel.h"
#include "smtk/simulation/cmb2d/ImportModel_xml.h"
#include "smtk/simulation/cmb2d/Metadata.h"

#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/operation/Manager.h"

#include "smtk/project/Project.h"

#include <cctype>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

#define LogInternalError(message)                                                                  \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(logger, "Internal Error: " << message);                                         \
    cleanup();                                                                                     \
    return this->createResult(ImportModel::Outcome::FAILED);                                       \
  } while (0)

#define LogError(message)                                                                          \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(logger, "Error: " << message);                                                  \
    cleanup();                                                                                     \
    return this->createResult(ImportModel::Outcome::FAILED);                                       \
  } while (0)

#define LogWarning(message)                                                                        \
  do                                                                                               \
  {                                                                                                \
    smtkErrorMacro(logger, "Warning: " << message);                                                \
  } while (0)

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

ImportModel::Result ImportModel::operateInternal()
{
  auto& logger = this->log();
  std::function<void(void)> cleanup = []() {}; // noop
  auto modelRoleStr = Metadata::roleToStr(Metadata::Role::MODEL);

  ImportModel::Result result = this->createResult(ImportModel::Outcome::SUCCEEDED);

  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::project::Project>();
  if (project == nullptr)
  {
    LogError("No project associated");
  }

  if (!project->resources().findByRole(modelRoleStr).empty())
  {
    LogError("Cannot add model, project already has a model");
  }

  // Get the import file
  fs::path modelPath;
  {
    auto fileItem = this->parameters()->findFile("filename");
    if (!fileItem->isSet())
    {
      LogError("Missing 2d Model input");
    }

    modelPath = fileItem->value();
    if (!fs::exists(modelPath))
    {
      LogError("Could not find import file " << modelPath.string());
    }
  }

  // Make a copy import file and store it in the projects asset directory
  int allowOverwrite = this->parameters()->find("overwrite")->isEnabled();
  fs::path copyPath;
  {
    fs::path assetPath = Metadata::getAssetPath(project);

    // Create an asset directory
    if (!fs::exists(assetPath))
    {
      if (!fs::create_directories(assetPath))
      {
        LogError("Could not create project asset directory");
      }
    }

    // Copy the file if it is not already an asset
    fs::path copyPath = assetPath / modelPath.filename();
    fs::path backupPath = assetPath / (modelPath.filename().string() + ".backup");
    if (copyPath != modelPath)
    {
      if (fs::exists(copyPath))
      {
        if (allowOverwrite)
        {
          // Make a backup just in case
          fs::copy(modelPath, backupPath);
        }
        else
        {
          LogError("Asset with name " << modelPath.filename() << " already exists");
        }
      }
      fs::copy_file(modelPath, copyPath);

      // Update cleanup to reverse the copy operation
      cleanup = [backupPath, copyPath]() {
        fs::remove(copyPath);
        if (fs::exists(backupPath))
        {
          fs::copy_file(backupPath, copyPath);
          fs::remove(backupPath);
        }
      };

      // Update the modelPath
      modelPath = copyPath;
    }
  }

  // Import the model
  {
    smtk::operation::Operation::Ptr importOp;
    smtk::resource::Resource::Ptr modelResource;

    std::string ext = modelPath.extension().string();
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    if (modelPath.extension() == ".ppg")
    {
      if (!(this->manager()))
      {
        LogInternalError("Could not find Operation Manager");
      }
      importOp = this->manager()->create("smtk::session::polygon::ImportPPG");
      if (!importOp)
      {
        LogError(
          "Could not create \"smtk::session::polygon::ImportPPG\"\n"
          << "\tThis is most likely due to the Polygon Session not being loaded");
      }
      importOp->parameters()->findFile("filename")->setValue(modelPath.string());
    }
    else
    {
      LogError("Unrecognized file extension " << ext);
    }

    auto importResult = importOp->operate();
    if (importResult->findInt("outcome")->value() != OP_SUCCEEDED)
    {
      LogError("Could not import model file");
    }

    modelResource = importResult->findResource("resource")->value();
    if (!modelResource)
    {
      LogError("Could not find Resource");
    }
    modelResource->properties().get<std::string>()["source"] = modelPath.string();

    // Add the resource to the project
    project->resources().add(modelResource, modelRoleStr);

    result->findResource("resource")->setValue(modelResource);
  }

  return result;
}

const char* ImportModel::xmlDescription() const
{
  return ImportModel_xml;
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
