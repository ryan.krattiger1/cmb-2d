<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="create" Label="CMB 2D Project - Create" BaseType="operation">
      <BriefDescription>
        Create a CMB Project for preparing 2D simulations (namely MinimalFEM)
      </BriefDescription>

      <ItemDefinitions>
        <Directory Name="location" Label="Project Directory" NumberOfRequiredValues="1">
          <BriefDescription>The folder in the local filesystem for storing project files.</BriefDescription>
        </Directory>
        <Void Name="overwrite" Label="OK to overwrite existing directory?" Optional="true" IsEnabledByDefault="false"/>
        <String Name="analysis-name" Label="Analysis" AdvanceLevel="1">
          <DefaultValue>Analysis1</DefaultValue>
        </String>
        <File Name="model-filename" Label="Model"
              Optional="true"
              IsEnabledByDefault="false"
              NumerberOfRequiredValues="1"
              ShouldExist="true"
              FileFilters="Planar Polygon Files (*.ppg)">
          <BriefDescription>A file containing a Model</BriefDescription>
        </File>
        <File Name="simulation-template" Label="Override Simulation Template"
              AdvanceLevel="1"
              ShouldExist="true"
              Optional="true"
              IsEnabledByDefault="false"
              FileFilters="CMB Template Files (*.sbt)">
          <BriefDescription>The simulation template to use in place of the default.</BriefDescription>
        </File>
      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(create>" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::project::Project"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
