set(test_sources
  testCreate.cxx
  testImportModel.cxx
  )

unit_tests(
  LABEL UnitTests
  SOURCES
    ${test_sources}
  LIBRARIES
    smtkCMB2D
)
