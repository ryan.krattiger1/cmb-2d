#include "smtk/simulation/cmb2d/plugin/pqCMB2DExtensionsAutoStart.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// ParaView Qt
#include "pqCoreUtilities.h"

pqCMB2DExtensionsAutoStart::pqCMB2DExtensionsAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqCMB2DExtensionsAutoStart::~pqCMB2DExtensionsAutoStart() {}

void pqCMB2DExtensionsAutoStart::startup()
{
  qtProjectRuntime::instance(pqCoreUtilities::mainWidget());
}

void pqCMB2DExtensionsAutoStart::shutdown() {}
