//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqCMB2DProjectMenu.h"

#include "behaviors/pqMesherBehavior.h"
#include "behaviors/pqProjectCloseBehavior.h"
#include "behaviors/pqProjectLoader.h"
#include "behaviors/pqProjectOpenBehavior.h"
#include "behaviors/pqProjectSaveBehavior.h"
#include "behaviors/pqProjectExportBehavior.h"
#include "behaviors/pqImportModelBehavior.h"
#include "behaviors/pqProjectNewBehavior.h"
// TODO
//#include "pqRecentProjectsMenu.h"
// Save as Behavior...

#include "smtk/simulation/cmb2d/Metadata.h"
using smtk::simulation::cmb2d::Metadata;

// SMTK
#include "smtk/model/Resource.h"
#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"

// ParaView
#include "pqApplicationCore.h"

#include <QAction>
#include <QDebug>
#include <QMenu>
#include <QString>
#include <QtGlobal> // qWarning()

//-----------------------------------------------------------------------------
pqCMB2DProjectMenu::pqCMB2DProjectMenu(QObject* parent)
  : Superclass(parent)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqCMB2DProjectMenu::~pqCMB2DProjectMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqCMB2DProjectMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize Project menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto newProjectBehavior = smtk::simulation::cmb2d::pqProjectNewBehavior::instance(this);
  auto loadProjectBehavior = smtk::simulation::cmb2d::pqProjectLoader::instance(this);
  auto saveProjectBehavior = smtk::simulation::cmb2d::pqProjectSaveBehavior::instance(this);
  auto closeProjectBehavior = smtk::simulation::cmb2d::pqProjectCloseBehavior::instance(this);

  auto importModelBehavior = smtk::simulation::cmb2d::pqImportModelBehavior::instance(this);
  auto mesherBehavior = smtk::simulation::cmb2d::pqMesherBehavior::instance(this);
  auto exportProjectBehavior = smtk::simulation::cmb2d::pqProjectExportBehavior::instance(this);

  // Initialize "New Project" action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new smtk::simulation::cmb2d::pqProjectNewReaction(m_newProjectAction);
  QObject::connect(
    newProjectBehavior,
    &smtk::simulation::cmb2d::pqProjectNewBehavior::projectCreated,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Open Project" action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction =
    new smtk::simulation::cmb2d::pqProjectOpenReaction(m_openProjectAction);

  // Initialize "Recent Projects" menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* menu = new QMenu();
  m_recentProjectsAction->setMenu(menu);
  //m_recentProjectsMenu = new pqSMTKRecentProjectsMenu(menu, menu);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Import Model" action
  m_importModelAction = new QAction(tr("Add Model..."), this);
  m_importModelAction->setEnabled(false);
  auto importModelReaction =
    new smtk::simulation::cmb2d::pqImportModelReaction(m_importModelAction);
  QObject::connect(
    importModelBehavior,
    &smtk::simulation::cmb2d::pqImportModelBehavior::modelImported,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  m_generateMeshAction = new QAction(tr("Generate Mesh..."), this);
  m_generateMeshAction->setEnabled(false);
  auto mesherReaction = new smtk::simulation::cmb2d::pqMesherReaction(m_generateMeshAction);
  QObject::connect(
    mesherBehavior,
    &smtk::simulation::cmb2d::pqMesherBehavior::meshGenerated,
    this,
    &pqCMB2DProjectMenu::onMeshGenerated);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Export Project" action
  m_exportProjectAction = new QAction(tr("Export Project"), this);
  auto exportProjectReaction = new smtk::simulation::cmb2d::pqProjectExportReaction(m_exportProjectAction);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Save Project" action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction =
    new smtk::simulation::cmb2d::pqProjectSaveReaction(m_saveProjectAction);

  // Initialize "Save As Project" action
  // m_saveAsProjectAction = new QAction(tr("Save As Project..."), this);
  // auto saveAsProjectReaction = new pqCMB2DProjectSaveAsReaction(m_saveAsProjectAction);

  // Insert separator
  {
    QAction* sep = new QAction("", this);
    sep->setSeparator(true);
  }

  // Initialize "Close Project" action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction =
    new smtk::simulation::cmb2d::pqProjectCloseReaction(m_closeProjectAction);
  QObject::connect(
    closeProjectBehavior,
    &smtk::simulation::cmb2d::pqProjectCloseBehavior::projectClosed,
    this,
    &pqCMB2DProjectMenu::onProjectClosed);

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  auto projectLoader = smtk::simulation::cmb2d::pqProjectLoader::instance(this);
  // Connect to project loader
  QObject::connect(
    projectLoader,
    &smtk::simulation::cmb2d::pqProjectLoader::projectOpened,
    this,
    &pqCMB2DProjectMenu::onProjectOpened);

  return true;
} // startup()

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::shutdown()
{
  // Reserved for future use.
}

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  smtkInfoMacro(smtk::io::Logger::instance(), __PRETTY_FUNCTION__);
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);

  auto model = project->resources().findByRole<smtk::model::Resource>(
    Metadata::getRoleStr(Metadata::Role::MODEL));
  auto mesh = project->resources().findByRole<smtk::model::Resource>(
    Metadata::getRoleStr(Metadata::Role::MESH));

  m_importModelAction->setEnabled(model.empty());
  m_generateMeshAction->setEnabled(!model.empty() && mesh.empty());

  m_exportProjectAction->setEnabled(!mesh.empty());
}

void pqCMB2DProjectMenu::onMeshGenerated()
{
  smtkInfoMacro(smtk::io::Logger::instance(), __PRETTY_FUNCTION__);
  m_newProjectAction->setEnabled(false);
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_saveProjectAction->setEnabled(true);
  m_closeProjectAction->setEnabled(true);
  m_exportProjectAction->setEnabled(true);

  m_importModelAction->setEnabled(false);
  m_generateMeshAction->setEnabled(false);
}

//-----------------------------------------------------------------------------
void pqCMB2DProjectMenu::onProjectClosed()
{
  m_newProjectAction->setEnabled(true);
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(false /* true */);
  m_saveProjectAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_exportProjectAction->setEnabled(false);
  m_importModelAction->setEnabled(false);
  m_generateMeshAction->setEnabled(false);
}
