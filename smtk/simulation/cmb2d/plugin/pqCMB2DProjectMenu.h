//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqCMB2DProjectMenu_h
#define smtk_simulation_cmb2d_plugin_pqCMB2DProjectMenu_h

#include <QActionGroup>

#include "smtk/simulation/cmb2d/qt/Exports.h"

#include "smtk/project/Project.h"

class QAction;

class pqSMTKRecentProjectsMenu;

/** \brief Adds a "CMB2D" menu to the application main window
  */
class SMTKCMB2DQTEXT_NO_EXPORT pqCMB2DProjectMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

signals:

public slots:
  void onProjectOpened(std::shared_ptr<smtk::project::Project> project);
  void onProjectClosed();
  void onMeshGenerated();

public:
  pqCMB2DProjectMenu(QObject* parent = nullptr);
  ~pqCMB2DProjectMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_newProjectAction = nullptr;
  QAction* m_openProjectAction;
  QAction* m_recentProjectsAction;
  QAction* m_saveProjectAction;
  // QAction* m_saveAsProjectAction;  // not supported
  QAction* m_closeProjectAction;
  QAction* m_exportProjectAction = nullptr;
  QAction* m_importModelAction;
  QAction* m_generateMeshAction;

  //pqSMTKRecentProjectsMenu* m_recentProjectsMenu;

  Q_DISABLE_COPY(pqCMB2DProjectMenu);
};

#endif // pqCMB2DProjectMenu_h
