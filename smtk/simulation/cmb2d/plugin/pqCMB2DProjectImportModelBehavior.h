//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqCMB2DProjectImportModelBehavior_h
#define smtk_simulation_cmb2d_plugin_pqCMB2DProjectImportModelBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

/// A reaction for importing a model into a project.
/// This class is HARD-CODED to only support importing a model for the
/// MODEL role.

class pqCMB2DProjectImportModelReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqCMB2DProjectImportModelReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqCMB2DProjectImportModelReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqCMB2DProjectImportModelBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqCMB2DProjectImportModelBehavior* instance(QObject* parent = nullptr);
  ~pqCMB2DProjectImportModelBehavior() override;

  void importModel();

signals:
  void modelImported(smtk::project::ProjectPtr);

protected:
  pqCMB2DProjectImportModelBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqCMB2DProjectImportModelBehavior);
};

#endif
