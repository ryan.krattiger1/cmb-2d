#include "smtk/simulation/cmb2d/plugin/pqCMB2DAutoStart.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"

// ParaView Qt
#include "pqCoreUtilities.h"

pqCMB2DAutoStart::pqCMB2DAutoStart(QObject* parent)
  : Superclass(parent)
{
}

pqCMB2DAutoStart::~pqCMB2DAutoStart() {}

void pqCMB2DAutoStart::startup()
{
  smtk::simulation::cmb2d::qtProjectRuntime::instance(pqCoreUtilities::mainWidget());
}

void pqCMB2DAutoStart::shutdown() {}
