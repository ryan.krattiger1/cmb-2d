#pragma once

#include <QObject>

class pqCMB2DExtensionsAutoStart : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqCMB2DExtensionsAutoStart(QObject* parent = nullptr);
  ~pqCMB2DExtensionsAutoStart() override;

  void startup();
  void shutdown();

private:
  Q_DISABLE_COPY(pqCMB2DExtensionsAutoStart);
};
