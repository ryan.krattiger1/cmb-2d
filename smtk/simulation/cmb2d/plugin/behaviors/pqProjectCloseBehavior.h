//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_cmb2d_plugin_pqProjectCloseBehavior_h
#define smtk_simulation_cmb2d_plugin_pqProjectCloseBehavior_h

#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

/// A reaction for writing a resource manager's state to disk.
class pqProjectCloseReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectCloseReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqProjectCloseReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqProjectCloseBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqProjectCloseBehavior* instance(QObject* parent = nullptr);
  ~pqProjectCloseBehavior() override;

  void closeProject();

signals:
  void projectClosed();

protected:
  pqProjectCloseBehavior(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqProjectCloseBehavior);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
