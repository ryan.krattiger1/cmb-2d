//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqProjectOpenBehavior.h"

#include "pqProjectLoader.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"
#include "pqWaitCursor.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QMessageBox>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

//-----------------------------------------------------------------------------
pqProjectOpenReaction::pqProjectOpenReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectOpenReaction::openProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Check settings for ProjectsRootFolder
  QString workspaceFolder;
  vtkSMStringVectorProperty* workspaceStringProp = nullptr;
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* workspaceProp = proxy->GetProperty("ProjectsRootFolder");
    workspaceStringProp = vtkSMStringVectorProperty::SafeDownCast(workspaceProp);
    if (workspaceStringProp != nullptr)
    {
      workspaceFolder = workspaceStringProp->GetElement(0);
    } // if (projectStringProp)
  }

  // Construct a file dialog for the user to select the project directory to load
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Select Project (Directory):"), workspaceFolder);
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(pqFileDialog::Directory);
  fileDialog.setShowHidden(true);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString directory = fileDialog.getSelectedFiles()[0];
  pqWaitCursor cursor;
  if (pqProjectLoader::instance()->load(server, directory) && (workspaceStringProp != nullptr))
  {
    // Update workspaceStringProp
    QDir dir(directory);
    if (dir.cdUp())
    {
      workspaceStringProp->SetElement(0, dir.absolutePath().toStdString().c_str());
    }
  }
} // openProject()

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
