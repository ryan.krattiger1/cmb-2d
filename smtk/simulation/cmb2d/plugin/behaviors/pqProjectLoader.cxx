//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqProjectLoader.h"

#include "smtk/simulation/cmb2d/Metadata.h"
#include "smtk/simulation/cmb2d/qt/qtProjectRuntime.h"
//#include "smtk/simulation/cmb2d/utility/ModelUtils.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/operators/Read.h"

// ParaView (client)
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqRecentlyUsedResourcesList.h"
#include "pqServer.h"
#include "pqServerConfiguration.h"
#include "pqServerConnectDialog.h"
#include "pqServerLauncher.h"
#include "pqServerManagerModel.h"
#include "pqServerResource.h"

// Qt
#include <QDebug>
#include <QDir>
#include <QMessageBox>
#include <QString>
#include <QTextStream>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

//-----------------------------------------------------------------------------
static pqProjectLoader* g_instance = nullptr;

//-----------------------------------------------------------------------------
pqProjectLoader* pqProjectLoader::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqProjectLoader(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

//-----------------------------------------------------------------------------
bool pqProjectLoader::canLoad(const pqServerResource& resource) const
{
  return resource.hasData(RECENTLY_USED_PROJECTS_TAG);
}

//-----------------------------------------------------------------------------
bool pqProjectLoader::load(pqServer* server, const QString& directoryPath)
{
  // Todo abort if qtProjectRuntime project is already set.

  // Set up project read operation
  smtk::operation::OperationPtr readOp;
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Find the project file
  QDir projectDir(directoryPath);
  QString filename =
    projectDir.dirName() + smtk::simulation::cmb2d::Metadata::PROJECT_FILE_EXTENSION.c_str();
  if (projectDir.exists(filename))
  {
    readOp = opManager->create<smtk::project::Read>();
    assert(readOp != nullptr);
  }
  else
  {
    QString msg;
    QTextStream qs(&msg);
    qs << "ERROR: project file (" << filename << ") not found in project folder ("
       << projectDir.canonicalPath() << ").";
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return false;
  }

  QString path = projectDir.absoluteFilePath(filename);
  readOp->parameters()->findFile("filename")->setValue(path.toStdString());

  auto result = readOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCEEDED)
  {
    qWarning() << readOp->log().convertToString().c_str();
    QString msg =
      "There was an error loading the project. Check the Output Messages view for more info.";
    QMessageBox::warning(pqCoreUtilities::mainWidget(), "Error", msg);
    return false;
  }

  // Add project to recently-used list
  pqServerResource serverResource = server->getResource();
  serverResource.addData(RECENTLY_USED_PROJECTS_TAG, "1");
  serverResource.setPath(directoryPath);

  pqRecentlyUsedResourcesList& mruList = pqApplicationCore::instance()->recentlyUsedResources();
  mruList.add(serverResource);
  mruList.save(*pqApplicationCore::instance()->settings());

#if 0
  // Make sure that model resources get rendered, which does *not* happen
  // if the resource panel is hidden.
  auto smtkResourceList = project->resources();
  for (auto smtkResource : smtkResourceList)
  {
    if (smtkResource->isOfType<smtk::model::Resource>())
    {
      pqSMTKResource* pvResource = wrapper->getPVResource(smtkResource);
      if (pvResource == nullptr)
      {
        pvResource = pqSMTKRenderResourceBehavior::instance()->createPipelineSource(smtkResource);
      }
      pqSMTKRenderResourceBehavior::instance()->renderPipelineSource(pvResource);
    } // if (model resource)
  }   // for (smtk resources)
#endif

  smtk::attribute::ResourceItemPtr projectItem = result->findResource("resource");
  auto resource = projectItem->value();
  assert(resource != nullptr);
  // std::cout << "Created resource type " << resource->typeName() << std::endl;
  auto project = std::dynamic_pointer_cast<smtk::project::Project>(resource);
  assert(project != nullptr);

  // Set resource "tags" (string properties used by toolbar)
  //smtk::simulation::cmb2d::ModelUtils modelUtils;
  //modelUtils.tagResources(project);

  qtProjectRuntime::instance()->setProject(project);
  qInfo() << "Opened project" << project->name().c_str();

  emit this->projectOpened(project);
  return true;
}

//-----------------------------------------------------------------------------
bool pqProjectLoader::load(const pqServerResource& resource)
{
  const pqServerResource server = resource.schemeHostsPorts();

  pqServerManagerModel* smModel = pqApplicationCore::instance()->getServerManagerModel();
  pqServer* pq_server = smModel->findServer(server);
  if (!pq_server)
  {
    int ret = QMessageBox::warning(
      pqCoreUtilities::mainWidget(),
      tr("Disconnect from current server?"),
      tr("The file you opened requires connecting to a new server. \n"
         "The current connection will be closed.\n\n"
         "Are you sure you want to continue?"),
      QMessageBox::Yes | QMessageBox::No);
    if (ret == QMessageBox::No)
    {
      return false;
    }
    pqServerConfiguration config_to_connect;
    if (pqServerConnectDialog::selectServer(
          config_to_connect, pqCoreUtilities::mainWidget(), server))
    {
      QScopedPointer<pqServerLauncher> launcher(pqServerLauncher::newInstance(config_to_connect));
      if (launcher->connectToServer())
      {
        pq_server = launcher->connectedServer();
      }
    }
  }

  if (pq_server)
  {
    return this->load(pq_server, resource.path());
  }

  return false;
}

//-----------------------------------------------------------------------------
pqProjectLoader::pqProjectLoader(QObject* parent)
  : Superclass(parent)
{
}

//-----------------------------------------------------------------------------
pqProjectLoader::~pqProjectLoader()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

} // namespace cmb2d
} // namespace simulation
} // namespace smtk
