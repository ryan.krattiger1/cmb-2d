#ifndef smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h
#define smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h

#include "smtk/simulation/cmb2d/qt/qtInstanced.h"
#include "pqReaction.h"

#include <QObject>

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class pqProjectExportReaction : public pqReaction
{
  Q_OBJECT
  using Superclass = pqReaction;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqProjectExportReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqProjectExportReaction)
};

class pqProjectExportBehavior : public qtInstanced<pqProjectExportBehavior>
{
  Q_OBJECT
  using Superclass = qtInstanced<pqProjectExportBehavior>;
  friend Superclass;
public:
  ~pqProjectExportBehavior() override;

  void exportSimulation();

signals:

protected:
  pqProjectExportBehavior(QObject* parent = nullptr);
};

}
}
}

#endif // smtk_simulation_cmb2d_plugin_behaviors_pqExportBehavior_h
