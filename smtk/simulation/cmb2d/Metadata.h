#ifndef smtk_simulation_cmb2d_Metadata_h
#define smtk_simulation_cmb2d_Metadata_h

#include "smtk/simulation/cmb2d/Exports.h"

#include "smtk/PublicPointerDefs.h"

#include <array>
#include <string>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

namespace smtk
{
namespace simulation
{
namespace cmb2d
{

class SMTKCMB2D_EXPORT Metadata
{
public:
  enum Role
  {
    ANALYSIS,
    MODEL,
    MESH,
    NROLE
  };

  static const std::string PROJECT_TYPENAME;
  static const std::string PROJECT_FILE_EXTENSION;
  static const std::string PROPERTY_PREFIX;
  static const std::array<std::string, Role::NROLE> ROLES;

  static std::string WORKFLOW_DIRECTORY;

  static std::string getRoleStr(Role role);
  static std::string roleToStr(Role role);
  static Role strToRole(std::string roleStr);

  static bool isUniqueRole(Role role);
  static bool isUniqueRole(std::string roleStr);

  static smtk::resource::ResourcePtr getRole(smtk::project::ProjectPtr project, Role role);
  static void addResourceToProject(
    smtk::project::ProjectPtr project,
    smtk::resource::ResourcePtr resource,
    Role role);

  static fs::path getAssetPath(smtk::project::ProjectPtr project);
  static fs::path getResourcePath(smtk::project::ProjectPtr project);
  static fs::path getProjectRoot(smtk::project::ProjectPtr project);
};

} // namespace cmb2d
} // namespace simulation
} // namespace smtk

#endif
