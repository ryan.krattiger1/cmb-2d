#include "smtk/simulation/minimalfem/Registrar.h"

#include "smtk/simulation/minimalfem/operations/Export.h"

#include "smtk/operation/Manager.h"

namespace smtk
{
namespace simulation
{
namespace minimalfem
{

namespace
{
using OperationList = std::tuple<Export>;
}

void Registrar::registerTo(const smtk::operation::ManagerPtr& manager)
{
  manager->registerOperations<OperationList>();
}

void Registrar::unregisterFrom(const smtk::operation::ManagerPtr& manager)
{
  manager->unregisterOperations<OperationList>();
}

} // namespace minimalfem
} // namespace simulation
} // namespace smtk
