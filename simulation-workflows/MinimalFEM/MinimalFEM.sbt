<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="4">
  <Categories>
    <Cat>MinimalFEM</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="elasticity" Label="Elasticity">
      <Categories>
        <Cat>MinimalFEM</Cat>
      </Categories>
      <ItemDefinitions>
        <Double Name="poisson-ratio" Label="Poisson's Ratio (&#x3BD;)">
          <DefaultValue>0.25</DefaultValue>
          <RangeInfo>
            <Min>-1.0</Min>
            <Max>0.5</Max>
          </RangeInfo>
        </Double>

        <Double Name="youngs-modulus" Label="Young's Modulus (MPa)">
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="constraint" Label="Constraint" Unique="true">
      <Categories>
        <Cat>MinimalFEM</Cat>
      </Categories>
      <AssociationsDef Name="associations" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>edge|vertex</MembershipMask>
      </AssociationsDef>
      <BriefDescription>Constrain vertex displacement</BriefDescription>
      <ItemDefinitions>
        <Int Name="constraint" Label="Displacement Constraint">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="X Direction">1</Value>
            <Value Enum="Y Direction">2</Value>
            <Value Enum="X and Y Directions">3</Value>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="load" Label="Load">
      <Categories>
        <Cat>MinimalFEM</Cat>
      </Categories>
      <AssociationsDef Name="associations" NumberOfRequiredValues="0" Extensible="true" Unique="false">
        <MembershipMask>edge|vertex</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <Double Name="load" Label="Point Load (N)" NumberOfRequiredValues="2">
          <ComponentLabels>
            <Label>X-Force: </Label>
            <Label>Y-Force: </Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Group" Name="MinimalFEM"
      TabPosition="North" FilterByAdvanceLevel="false" FilterByCategory="false">
      <Views>
        <View Title="Elasticity" />
        <View Title="Constraints" />
        <View Title="Loads" />
      </Views>
    </View>

    <View Type="Instanced" Title="Elasticity">
      <InstancedAttributes>
        <Att Name="Elasticity" Type="elasticity" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="Constraints">
      <AttributeTypes>
        <Att Type="constraint" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Loads">
      <AttributeTypes>
        <Att Type="load" />
      </AttributeTypes>
    </View>

  </Views>

</SMTK_AttributeResource>
