from __future__ import print_function

import smtk
import smtk.attribute
import smtk.common
import smtk.io
import smtk.mesh
import smtk.operation

import datetime
import inspect
import os
import sys
sys.dont_write_bytecode


# Add the directory containing this file to the python module search list
source_file = os.path.abspath(inspect.getfile(inspect.currentframe()))
print('source_file', source_file)
sys.path.insert(0, os.path.dirname(source_file))
# Make sure __file__ is set when using modelbuilder
__file__ = source_file
print('loading', os.path.basename(__file__))

spec_string = """
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="export" BaseType="operation" Label="Export to Truchas" Version="1">
      <BriefDescription>
        Write MinimalFEM input file.
      </BriefDescription>
      <ItemDefinitions>
        <Resource Name="attributes" Label="Attributes" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <Resource Name="mesh" Label="Mesh" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
        <File Name="output-file" Label="Output File"
          FileFilters="Input files (*.inp);;All files (*.*)" Version="0">
        </File>
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  </SMTK_AttributeResource>
"""


class Export(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)
        self.mesh_points = None
        self.out = None

    def name(self):
        return "Export MinimalFEM"

    def operateInternal(self):
        try:
            success = self._write_mfem()
        except:
            print('Error', self.log().convertToString())
            #smtk.ErrorMessage(self.log(), sys.exc_info()[0])
            raise

        # Return with success
        result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        result.find('success').setValue(success)
        print('result', result)
        return result

    def createSpecification(self):
        spec = self.createBaseSpecification()

        # Load export definitions
        reader = smtk.io.AttributeReader()
        result = reader.readContents(spec, spec_string, self.log())
        print('reader result:', result)

        resultDef = spec.createDefinition('test result', 'result')
        successDef = smtk.attribute.IntItemDefinition.New('success')
        resultDef.addItemDefinition(successDef)

        return spec

    def _write_mfem(self):
        params = self.parameters()
        logger = self.log()

        # Get sim attributes
        sim_atts = params.findResource('attributes').value()
        mesh = params.findResource('mesh').value()

        self.model_resource = None
        # Obtain model resource from mesh or attribute resource
        # Try mesh resource first
        model_resource = mesh.classifiedTo()
        if model_resource is None:
            print('WARNING mesh not classified on a model')

            # Try attribute resource next
            assocs = sim_atts.associations()
            if not assocs:
                print('WARNING: No models associated to attribute resource')
            else:
                for resource in assocs:
                    if isinstance(resource, smtk.model.Model):
                        if model_resource is None:
                            model_resource = smtk.model.Resource(resource)
                        else:
                            print(
                                'WARNING: More than one model associated '
                                'to attribute resource')
                            model_resource = None
                            break

        if model_resource is None:
            # Try attributes next
            atts = sim_atts.attributes()
            for att in atts:
                assocs = att.associations()
                if assocs:
                    pers_obj = assocs.value(0)
                    resource = pers_obj.resource()
                    model_resource = smtk.model.Resource.CastTo(resource)
                    if model_resource is not None:
                        break

        # No model_resource - cannot export
        if model_resource is None:
            raise RuntimeError('Unable to export; cannot find model resource')
        self.model_resource = model_resource

        self.mesh_points = mesh.points()
        print('mesh_points', self.mesh_points.size())

        # Get output filename
        output_file_item = params.findFile('output-file')
        output_file = output_file_item.value(0)
        output_dir = os.path.dirname(output_file)

        # Create output folder if needed
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        completed = False
        with open(output_file, 'w') as out:
            self._write_elasticity(sim_atts, out)
            self._write_mesh(mesh, out)
            self._write_constraints(sim_atts, mesh, out)
            self._write_loads(sim_atts, mesh, out)

            completed = True

        # (else)
        return completed

    def _write_elasticity(self, sim_atts, out):
        """"""
        att = sim_atts.findAttribute('Elasticity')
        if not att.isValid():
            raise RuntimeError('Elasticiy settings not valid')

        pr = att.findDouble('poisson-ratio').value()
        ym = att.findDouble('youngs-modulus').value()
        out.write('{} {}\n'.format(pr, ym))

    def _write_mesh(self, mesh, out):
        """"""
        print('mesh', mesh)
        num_points = self.mesh_points.size()

        # Write points (nodes)
        out.write('{}\n'.format(num_points))

        # Use point visitor to write mesh points
        class PointVisitor(smtk.mesh.PointForEach):
            def __init__(self, out):
                smtk.mesh.PointForEach.__init__(self)
                self.out = out

            def forPoints(self, pointIds, xyz, doModify):
                for i in range(num_points):
                    n = i * 3
                    out.write('{} {}\n'.format(xyz[n], xyz[n+1]))

        point_visitor = PointVisitor(out)
        smtk.mesh.for_each(self.mesh_points, point_visitor)

        # Write faces using tessellation
        faces = mesh.cells(smtk.mesh.Dims2)
        num_faces = faces.size()
        out.write('{}\n'.format(num_faces))

        tess = smtk.mesh.Tessellation(False, False)
        faces = self.model_resource.findEntitiesOfType(smtk.model.FACE, True)
        for face in faces:
            meshset = mesh.findAssociatedMeshes(face, smtk.mesh.Dims2)
            tess.extract(meshset, self.mesh_points)
            conn = tess.connectivity()
            num_triangles = int(len(conn) / 3)
            # print('conn, length', len(conn), 'num_tris', num_triangles)
            idx = 0
            for i in range(num_triangles):
                # print('  idx', idx)
                out.write('{} {} {}\n'.format(
                    conn[idx], conn[idx+1], conn[idx+2]))
                idx += 3

    def _write_constraints(self, sim_atts, mesh, out):
        """"""
        tess = smtk.mesh.Tessellation(False, False)

        atts = sim_atts.findAttributes('constraint')
        if not atts:
            raise RuntimeError('No displacement constraints specified')

        # Build lines in memory, because we need the final count
        output_lines = []

        # Todo use dict of <pid, constraint> to avoid duplicates
        for att in atts:
            # Get constraint type
            constraint = att.findInt('constraint').value()
            # Find all mesh points
            ref_item = att.associations()
            for i in range(ref_item.numberOfValues()):
                model_ent = ref_item.value(i)
                if model_ent.dimension() == 0:
                    vertex = smtk.model.Vertex(model_ent)
                    pid = self._find_mesh_vertex_id(vertex, mesh)
                    if pid is not None:
                        output_lines.append('{} {}\n'.format(pid, constraint))
                    print('Skipping vertex constraints for now')
                    continue

                # (else)
                edge = smtk.model.Edge(model_ent)
                meshset = mesh.findAssociatedMeshes(edge, smtk.mesh.Dims1)
                if meshset.is_empty():
                    print(
                        'WARNING: meshset is empty for model ent', edge.name())
                    continue

                tess.extract(meshset, self.mesh_points)
                conn = tess.connectivity()
                point_set = set()
                for pid in conn:
                    point_set.add(pid)
                for pid in point_set:
                    output_lines.append('{} {}\n'.format(pid, constraint))

        if not output_lines:
            raise RuntimeError(
                'No displacement constraints associated to the model')

        # Can now write the section
        out.write('{}\n'.format(len(output_lines)))
        out.write(''.join(output_lines))

    def _write_loads(self, sim_atts, mesh, out):
        tess = smtk.mesh.Tessellation(False, False)

        atts = sim_atts.findAttributes('load')
        if not atts:
            raise RuntimeError('No loads specified')

        # Build lines in memory, because we need the final count
        output_lines = []

        # Todo use dict of <pid, constraint> to avoid duplicates
        for att in atts:
            # Get load value
            load_item = att.findDouble('load')
            load_data = [load_item.value(0), load_item.value(1)]

            # Find all mesh points
            ref_item = att.associations()
            for i in range(ref_item.numberOfValues()):
                model_ent = ref_item.value(i)
                if model_ent.dimension() == 0:
                    vertex = smtk.model.Vertex(model_ent)
                    pid = self._find_mesh_vertex_id(vertex, mesh)
                    if pid is not None:
                        output_lines.append(
                            '{} {} {}\n'.format(pid, *load_data))
                    continue

                # (else)
                edge = smtk.model.Edge(model_ent)
                meshset = mesh.findAssociatedMeshes(edge, smtk.mesh.Dims1)
                if meshset.is_empty():
                    print(
                        'WARNING: meshset is empty for model ent', edge.name())
                    continue

                tess.extract(meshset, self.mesh_points)
                conn = tess.connectivity()
                point_set = set()
                for pid in conn:
                    point_set.add(pid)
                for pid in point_set:
                    output_lines.append('{} {} {}\n'.format(pid, *load_data))

        if not output_lines:
            raise RuntimeError(
                'No displacement constraints associated to the model')

        # Can now write the section
        out.write('{}\n'.format(len(output_lines)))
        out.write(''.join(output_lines))

    def _find_mesh_vertex_id(self, model_vertex, mesh):
        """Find mesh vertex from intersection of mesh edge tesselations"""
        tess = smtk.mesh.Tessellation(False, False)

        # Traverse model edges and their tessellations
        mesh_ids = set()
        for model_edge in model_vertex.edges():
            meshset = mesh.findAssociatedMeshes(model_edge, smtk.mesh.Dims1)
            if meshset.is_empty():
                continue
            tess.extract(meshset, self.mesh_points)
            conn = tess.connectivity()
            if not conn:
                continue

            # Get first & last mesh vertex ids
            mv_ids = set([conn[0], conn[-1]])
            if not mesh_ids:
                mesh_ids = mv_ids
            else:
                mesh_ids &= mv_ids

        # Result should contain 1 mesh vertex
        if len(mesh_ids) == 1:
            return mesh_ids.pop()
        # (else)
        print('Failed to find mesh id for model vertex', model_vertex.id())
        return None


if __name__ == '__main__':
    exporter = Export()
    spec = exporter.createSpecification()

    spec_filename = 'spec.sbi'
    logger = smtk.io.Logger()
    writer = smtk.io.AttributeWriter()
    err = writer.write(spec, spec_filename, logger)
    if err:
        print('Error writing spec file', spec_filename)
    else:
        print('Wrote', spec_filename)
